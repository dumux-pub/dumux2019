// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH
#define DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH

#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>

namespace Dumux
{

/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class FVGridGeometry, class SinglePhaseTransmissibilityLaw, class Scalar>
class RandomNetWorkSpatialParams : public PNMOnePSpatialParams<FVGridGeometry, SinglePhaseTransmissibilityLaw, Scalar>
{
    using ParentType = PNMOnePSpatialParams<FVGridGeometry, SinglePhaseTransmissibilityLaw, Scalar>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using PermeabilityType = Scalar;
    using ParentType::ParentType;

    //! Workaround to halve the pore body volume at the coupling interface.
    //! Could also be done by adjusting the actual volume in the grid file.
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        const auto yMax = this->fvGridGeometry().bBoxMax()[1];
        if (globalPos[1] > yMax - 1e-10)
            return 0.5;
        else
            return 1;
    }
};

} // namespace Dumux

#endif
