// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>

#include <dumux/mixeddimension/boundary/freeflowpnm/couplingmanager.hh>
#include <dumux/io/grid/snappygridcreator.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/porenetworkgridcreator.hh>

#include "pnmspatialparams.hh"
#include "pnmproblem.hh"
#include "freeflowproblem.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeFlowTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PNMOnePTypeTag>;
    using type = Dumux::FreeFlowPNMCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PNMOnePTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeFlowTypeTag, Properties::TTag::FreeFlowTypeTag, TypeTag>;
    using type = Dumux::FreeFlowPNMCouplingManager<Traits>;
};


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeFlowTypeTag>
{
private:
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using BulkTypeTag = Properties::TTag::FreeFlowTypeTag;
    using LowDimTypeTag = Properties::TTag::PNMOnePTypeTag;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using LowDimGridManager = Dumux::PoreNetworkGridCreator<2>;
    LowDimGridManager lowDimGridManager;
    lowDimGridManager.init("PNM"); // pass parameter group

    using BulkGridManager = Dumux::SnappyGridCreator<2, LowDimGridManager>;
    std::vector<double> auxiliaryPositions;
    BulkGridManager bulkGridManager;
    bulkGridManager.init(lowDimGridManager.grid(), *(lowDimGridManager.getGridData()), auxiliaryPositions, "FreeFlow");

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using BulkFVGridGeometry = GetPropType<BulkTypeTag, Properties::FVGridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    bulkFvGridGeometry->update();
    using LowDimFVGridGeometry = GetPropType<LowDimTypeTag, Properties::FVGridGeometry>;
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    lowDimFvGridGeometry->update(*lowDimGridManager.getGridData());

    // the mixed dimension type traits
    using Traits = StaggeredMultiDomainTraits<BulkTypeTag, BulkTypeTag, LowDimTypeTag>;

    // the coupling manager
    using CouplingManager = FreeFlowPNMCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the indices
    constexpr auto bulkCellCenterIdx = CouplingManager::bulkCellCenterIdx;
    constexpr auto bulkFaceIdx = CouplingManager::bulkFaceIdx;
    constexpr auto lowDimIdx = CouplingManager::lowDimIdx;

    GetPropType<BulkTypeTag, Properties::FluidSystem>::init();


    // get some time loop parameters
    using Scalar = GetPropType<BulkTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    timeLoop->setPeriodicCheckPoint(getParam<Scalar>("TimeLoop.EpisodeLength"));

    // the problem (initial and boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, couplingManager);

    // the spatial parameters
    using LowDimSpatialParams = GetPropType<LowDimTypeTag, Properties::SpatialParams>;
    auto lowDimspatialParams = std::make_shared<LowDimSpatialParams>(lowDimFvGridGeometry);

    using LowDimProblem = GetPropType<LowDimTypeTag, Properties::Problem>;
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, lowDimspatialParams, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[bulkCellCenterIdx].resize(bulkFvGridGeometry->numCellCenterDofs());
    sol[bulkFaceIdx].resize(bulkFvGridGeometry->numFaceDofs());
    sol[lowDimIdx].resize(lowDimFvGridGeometry->numDofs());

    auto bulkSol = partial(sol, bulkCellCenterIdx, bulkFaceIdx);
    auto solOld = sol;

    couplingManager->init(bulkProblem, lowDimProblem, sol);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    bulkGridVariables->init(bulkSol);
    using LowDimGridVariables = GetPropType<LowDimTypeTag, Properties::GridVariables>;
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // pass the grid variables to the coupling manager
    couplingManager->setGridVariables(std::make_tuple(bulkGridVariables->cellCenterGridVariablesPtr(),
                                                      bulkGridVariables->faceGridVariablesPtr(),
                                                      lowDimGridVariables));

    FluxOverSurface<BulkGridVariables,
                    decltype(bulkSol),
                    GetPropType<BulkTypeTag, Properties::ModelTraits>,
                    GetPropType<BulkTypeTag, Properties::LocalResidual>> flux(*bulkGridVariables, bulkSol);
    bulkProblem->setPlanes(flux, auxiliaryPositions);

    // intialize the vtk output module
    const auto bulkName = getParam<std::string>("Problem.Name") + "_" + bulkProblem->name();
    const auto lowDimName = getParam<std::string>("Problem.Name") + "_" + lowDimProblem->name();

    StaggeredVtkOutputModule<BulkGridVariables, decltype(bulkSol)> bulkVtkWriter(*bulkGridVariables, bulkSol, bulkName);
    GetPropType<BulkTypeTag, Properties::VtkOutputFields>::initOutputModule(bulkVtkWriter);
    bulkVtkWriter.addVolumeVariable([](const auto& v){ return v.pressure()-1e5; }, "delP");

    bulkVtkWriter.write(0.0);

    PNMVtkOutputModule<LowDimTypeTag> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimName);
    GetPropType<LowDimTypeTag, Properties::VtkOutputFields>::initOutputModule(lowDimVtkWriter);

    auto diffusiveFlux = [](const auto& fluxVars, const auto& fluxVarsCache)
    {
        return fluxVars.molecularDiffusionFlux(0)[1];
    };
    lowDimVtkWriter.addFluxVariable(diffusiveFlux, "diffusiveFlux");

    lowDimVtkWriter.write(0.0);


    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkProblem, bulkProblem, lowDimProblem),
                                                 std::make_tuple(bulkFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 bulkFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 lowDimFvGridGeometry),
                                                 std::make_tuple(bulkGridVariables->cellCenterGridVariablesPtr(),
                                                                 bulkGridVariables->faceGridVariablesPtr(),
                                                                 lowDimGridVariables),
                                                 couplingManager,
                                                 timeLoop);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;
        bulkGridVariables->advanceTimeStep();
        lowDimGridVariables->advanceTimeStep();

        flux.calculateMassOrMoleFluxes();
        bulkProblem->printFluxes(flux);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        bulkVtkWriter.write(timeLoop->time());
        lowDimVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(bulkGridView.comm());
    timeLoop->finalize(lowDimGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
