//+
SetFactory("OpenCASCADE");
Cylinder(1) = {0, 0, -0.1, 0, 0, 0.1, 0.05, 2*Pi};
p() = PointsOf{ Volume{1}; };
Characteristic Length {p()} = 0.002;
