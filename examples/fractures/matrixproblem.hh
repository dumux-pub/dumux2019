// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_FRACTUREEXAMPLE_MATRIX_PROBLEM_HH
#define DUMUX_FRACTUREEXAMPLE_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// we want to simulate nitrogen gas transport in a water-saturated medium
#include <dumux/material/fluidsystems/h2on2.hh>

// We are using the framework for models that consider coupling
// across the element facets of the bulk domain. This has some
// properties defined, which we have to inherit here. In this
// exercise we want to use a cell-centered finite volume scheme
// with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {

// create the type tag node
namespace TTag {
struct MatrixProblem { using InheritsFrom = std::tuple<TwoP>; };
struct MatrixProblemTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, MatrixProblem>; };
struct MatrixProblemMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, MatrixProblem>; };
struct MatrixProblemBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, MatrixProblem>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblem> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblem> { using type = MatrixSubProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblem>
{
    using type = MatrixSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                      GetPropType<TypeTag, Properties::Scalar> >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblem>
{
    using type = Dumux::FluidSystems::H2ON2< GetPropType<TypeTag, Properties::Scalar>,
                                             FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/true> >;
};

} // end namespace Properties

/*!
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx
    };

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , boundarySaturation_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundarySaturation"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        const auto yMax = this->fvGridGeometry().bBoxMax()[1];
        if ((globalPos[1] < 1e-6 && globalPos[0] > 25.0 && globalPos[0] < 50.0)
            || (globalPos[1] > yMax - 1e-6 && globalPos[0] > 75.0))
            values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated. If this
        // permeability is lower than that of the matrix, this approach is able to
        // represent the resulting pressure jump across the fracture. If Dirichlet is set,
        // the pressure jump across the fracture is neglected and the pressure inside
        // the fracture is directly applied at the interface between fracture and matrix.
        // This assumption is justified for highly-permeable fractures, but lead to erroneous
        // results for low-permeable fractures.
        // Per default, we consider "open" fractures for which we cannot define a normal permeability
        // and for which the pressure jump across the fracture is neglectable. Thus, we set
        // the interior boundary conditions to Dirichlet.
        values.setAllDirichlet();

        // we need to obtain the domain marker of the fracture element that is coupled to this face
        // therefore we first get the fracture problem from the coupling manager. For this test, we
        // know that the fracture domain id is 1 (see main file)
        static constexpr auto fractureDomainId = Dune::index_constant<1>();
        const auto& fractureProblem = couplingManager().problem(fractureDomainId);

        // use helper function in coupling manager to obtain the element this face couples to
        const auto fractureElement = couplingManager().getLowDimElement(element, scvf);

        // obtain marker from the spatial params of the fracture problem
        const auto fractureElementMarker = fractureProblem.spatialParams().getElementDomainMarker(fractureElement);

        // now define Neumann coupling on those elements that are defined as blocking
        // fractures, i.e. marker == 2, see .geo file in grids folder
        if (fractureElementMarker == 2)
            values.setAllNeumann();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        // initialize values with the initial conditions
        auto values = initialAtPos(globalPos);

        // In the unmodified state of the exercise nitrogen is in contact
        // with the domain on the center half of the lower boundary
        if (globalPos[1] < 1e-6 && globalPos[0] > 25.0 && globalPos[0] < 50.0)
        {
            static const double p = getParam<double>("Matrix.Problem.BoundaryOverPressure");
            values[saturationIdx] = boundarySaturation_;
            values[pressureIdx] += p;
        }

        return values;
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is equal
        // to the maximum y-coordinate
        const auto domainHeight = this->fvGridGeometry().bBoxMax()[1];

        // we assume a constant water density of 1000 for initial conditions!
        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = 1e5 - (domainHeight - globalPos[1])*densityW*g[1];
        values[saturationIdx] = 0.0;
        return values;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar boundarySaturation_;
};

} // end namespace Dumux

#endif
