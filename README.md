# Dumux 3

Welcome to the dumux pub table Dumux2019 (DuMu<sup>x</sup> version 3)
======================================================

This module contains the source code for the three examples in the
paper

__T. Koch, D. Gläser, K. Weishaupt et al. (2019)__, _DuMu<sup>x</sup> 3 - an open-source simulator for solving flow
and transport problems in porous media with a focus
on model coupling._ [ArXiv preprint]() 

For building from source create a folder and download `install.sh` from this repository.
```bash
chmod +x install.sh
./install.sh
```
will download configure and compile all dune dependencies. Furthermore you need to have the following basic requirement installed

* CMake 2.8.12
* C, C++ compiler (C++14 required)
* Fortran compiler (gfortran)
* UMFPack from SuiteSparse

There are three examples in the folders
```
Dumux2019/build-cmake/examples/freeflowporenetwork
Dumux2019/build-cmake/examples/fractures
Dumux2019/build-cmake/examples/rootsoil
```

All examples are compiled by the following commands

```bash
cd Dumux2019/build-cmake
make -j4 build_tests
```

Run an example
```bash
cd Dumux2019/build-cmake/examples/rootsoil
./test_rootsoiltracer
```
