// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        diffusive mass fluxes due to molecular diffusion with Fick's law.
 */
#ifndef DUMUX_PNM_FICKS_LAW_HH
#define DUMUX_PNM_FICKS_LAW_HH

#include <dumux/common/math.hh>
#include <dumux/common/properties.hh>

namespace Dumux
{

/*!
 * \ingroup PNMFicksLaw
 * \brief Specialization of Fick's Law for the pore-network model.
 */
template <class TypeTag>
class PNMFicksLaw
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using GridView = GetPropType<TypeTag, Properties::GridView>;

    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int numComponents = GetPropType<TypeTag, Properties::ModelTraits>::numFluidComponents();

    using ComponentFluxVector = Dune::FieldVector<Scalar, numComponents>;

public:
    static ComponentFluxVector flux(const Problem& problem,
                                    const Element& element,
                                    const FVElementGeometry& fvGeometry,
                                    const ElementVolumeVariables& elemVolVars,
                                    const SubControlVolumeFace& scvf,
                                    const int phaseIdx,
                                    const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        ComponentFluxVector componentFlux(0.0);

        // get inside and outside diffusion tensors and calculate the harmonic mean
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

        const auto& fluxVarsCache = elemFluxVarsCache[scvf];
        const Scalar molarDensity = 0.5 * (insideVolVars.molarDensity(phaseIdx) + outsideVolVars.molarDensity(phaseIdx));
        const Scalar throatLength = fluxVarsCache.throatLength();
        const Scalar phaseCrossSectionalArea = fluxVarsCache.throatCrossSectionalArea(phaseIdx);

        for (int compIdx = 0; compIdx < numComponents; compIdx++)
        {
            if(compIdx == phaseIdx)
                continue;

            auto insideD = insideVolVars.diffusionCoefficient(phaseIdx, compIdx);
            auto outsideD = outsideVolVars.diffusionCoefficient(phaseIdx, compIdx);

            // scale by extrusion factor
            insideD *= insideVolVars.extrusionFactor();
            outsideD *= outsideVolVars.extrusionFactor();


            // the resulting averaged diffusion tensor
            const auto D = Dumux::harmonicMean(insideD, outsideD);

            const Scalar insideMoleFraction = insideVolVars.moleFraction(phaseIdx, compIdx);
            const Scalar outsideMoleFraction = outsideVolVars.moleFraction(phaseIdx, compIdx);

            componentFlux[compIdx] = molarDensity * (insideMoleFraction - outsideMoleFraction) / throatLength * D * phaseCrossSectionalArea;
            componentFlux[phaseIdx] -= componentFlux[compIdx];
        }
        return componentFlux;
    }

};
} // end namespace

#endif
