#!/bin/sh

# clone the git modules
git clone -b releases/2.6 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.6 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.6 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.6 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.6 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.6 https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone -b releases/2.6 https://gitlab.dune-project.org/extensions/dune-foamgrid.git
git clone -b releases/3.0 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/dumux2019.git

# configure and build
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all